use std::env;
use gst::glib;
use gst::prelude::*;
use once_cell::sync::Lazy;

static CAT: Lazy<gst::DebugCategory> = Lazy::new(|| {
    gst::DebugCategory::new(
        "sandbox",
        gst::DebugColorFlags::empty(),
        None,
    )
});


fn main() {
    gst::init().unwrap();

    gst::debug_set_threshold_from_string("sandbox:6", true);


    let args: Vec<_> = env::args().collect();
    let uri: &str = if args.len() == 2 {
        args[1].as_ref()
    } else {
        println!("Usage: discoverer uri");
        std::process::exit(-1)
    };

    let timeout: gst::ClockTime = gst::ClockTime::from_seconds(15);
    gst::info!(CAT, "Starting discovery");
    let discoverer = gst_pbutils::Discoverer::new(timeout).expect("Failed to create discoverer");
    discoverer.connect_closure("load-serialized-info", false,
        glib::closure!(|_discoverer: gst_pbutils::Discoverer, uri: &str| {
            gst::info!(CAT, "Retrieving: {uri:?}");

            None::<gst_pbutils::DiscovererInfo>
        })
    );

    let info = discoverer.discover_uri(uri).expect("Failed to discover URI");

    gst::info!(CAT, "Discovered: {info:?}");
}
