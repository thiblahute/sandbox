use std::sync::{Arc, Mutex};
use std::collections::HashMap;
use std::env;
use gst::glib;
use gst::prelude::*;
use ges::prelude::*;
use std::time::Instant;


fn lookup_discoverer_info(cache: Arc<Mutex<(i32, HashMap<String, Vec<u8>>)>>, uri: &str) -> Option<gst_pbutils::DiscovererInfo> {
        println!("  ---> load_serialized_info: Trying to load serialized info for {}", uri);

        cache.lock().unwrap().0 += 1;
        if let Some(variant_data) = cache.lock().unwrap().1.get(uri) {
            let variant = glib::Variant::from_data::<glib::Variant, _>(variant_data);

            println!("  ===> Reusing DiscoverInfo from previously saved variant");
            return Some(gst_pbutils::DiscovererInfo::from_variant(&variant));
        }

        None::<gst_pbutils::DiscovererInfo>
}

fn main() {
    gst::init().unwrap();
    ges::init().unwrap();

    gst::debug_set_threshold_from_string("sandbox:6", true);

    let args: Vec<_> = env::args().collect();
    let uri = if args.len() == 2 {
        args[1].clone()
    } else {
        eprintln!("Usage: discoverer uri");
        std::process::exit(-1)
    };

    let load_asset_info = Arc::new(Mutex::new((0 /* Number of time load-serialized-info was called */, HashMap::new())));
    ges::DiscovererManager::default().connect_load_serialized_info(glib::clone!(@strong load_asset_info => move |_manager, uri| {
        lookup_discoverer_info(load_asset_info.clone(), &uri)
    }));


    println!("\n=> Requesting asset");
    let now = Instant::now();
    let asset = ges::UriClipAsset::request_sync(&uri).expect("Could not create asset");
    println!("  ---> Asset discovered in {:?}", now.elapsed());

    // Serialize the GstDiscovererInfo and save it into our "database"
    let disco_info = asset.info();
    let variant = disco_info.to_variant(gst_pbutils::DiscovererSerializeFlags::all().to_owned());
    load_asset_info.lock().unwrap().1.insert(uri.clone(), variant.data().to_owned());
    assert!(load_asset_info.lock().unwrap().0 == 1);

    println!("\n=> Re requesting asset (which will hit the GES internal cache");
    let now = Instant::now();
    let _ = ges::UriClipAsset::request_sync(&uri).expect("Could not create asset");
    println!("  ---> Asset request from cache in {:?}", now.elapsed());

    print!("\n=> Deiniting GES (which will clear its cache)");
    unsafe { ges::deinit(); }
    ges::init().unwrap();

    // We need to re-register our load-serialized-info callback as initializing destroyed previous one
    ges::DiscovererManager::default().connect_load_serialized_info(glib::clone!(@strong load_asset_info => move |_manager, uri| {
        lookup_discoverer_info(load_asset_info.clone(), &uri)
    }));

    print!("\n\n=> Re requesting asset, which will trigger our own GstDiscovererInfo serialization cache");
    let now = Instant::now();
    // Check that it also works with async API
    let asset = glib::MainContext::ref_thread_default().block_on(
         ges::Asset::request_future(ges::UriClip::static_type(), Some(&uri))
    ).expect("Could not create asset");
    println!("  ---> Asset RE-discovered in {:?}", now.elapsed());

    assert_eq!(load_asset_info.lock().unwrap().0, 2);

    println!("\n\n* File duration: {:?}", asset.downcast::<ges::UriClipAsset>().unwrap().info().duration());
}

